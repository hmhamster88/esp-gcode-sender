import Communicator from './communicator.js';

window.onload = function () {
    const communicator = new Communicator('ws://' + window.location.host + '/ws');
    const sendInput = $('#text-to-send');
    const receiveInput = $('#received-text');

    communicator.on('message', message => {
        receiveInput.val(message);
    });

    communicator.on('connected', () => {
        console.log('Connected');
    });

    $('#send-button').click(() => {
        const text = sendInput.val();
        communicator.send(text)
    });
};
