export default class Communicator {
    constructor (address) {
        this.socket = new WebSocket(address, 'echo-protocol');
        this.connected = false;
        this.socket.onopen = () => {
            this.connected = true;
            this.fireEvent('connected');
        };
        this.socket.onmessage = (message) => {
            this.fireEvent('message', message.data);
        };
        this.listeners = [];
    }

    send (data) {
        this.socket.send(data);
    }

    on (eventName, listener) {
        let evListeners = this.listeners[eventName];
        if (!evListeners) {
            evListeners = this.listeners[eventName] = [];
        }
        evListeners.push(listener);
    }

    fireEvent (eventName, arg) {
        const evListeners = this.listeners[eventName];
        if (evListeners) {
            for (const listener of evListeners) {
                listener(arg);
            }
        }
    }
}